from gym.envs.registration import register

register(
    id='RMSA-v0',
    entry_point='optical_rl_gym.envs:RMSAEnv',
)

register(
     id='RBMLSA-v0',
    entry_point='optical_rl_gym.envs:RBMLSAEnv',
)

register(
    id='DeepRMSA-v0',
    entry_point='optical_rl_gym.envs:DeepRMSAEnv',
)

register(
     id='DeepRBMLSA-v0',
     entry_point='optical_rl_gym.envs:DeepRBMLSAEnv',
)

register(
     id='DeepRBMLSACR-v0',
     entry_point='optical_rl_gym.envs:DeepRBMLSACrEnv',
)

register(
    id='RWA-v0',
    entry_point='optical_rl_gym.envs:RWAEnv',
)

register(
    id='QoSConstrainedRA-v0',
    entry_point='optical_rl_gym.envs:QoSConstrainedRA',
)
