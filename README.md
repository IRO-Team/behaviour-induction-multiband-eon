# Optical RL-Gym

Two new environments are implemented and tested in the [Optical RL-Gym toolkit](https://github.com/carlosnatalino/optical-rl-gym.git) to allocate resources in dynamic multi-band elastic optical networks. Results show TRPO as the best performing agent, with consistent performance in different scenarios.

<a href="#installation"><h2>Installation</h2></a>

You can install the Optical RL-Gym and these new environments with the code below:

```bash
git clone https://gitlab.com/IRO-Team/optical-rl-gym-multiband.git
cd optical-rl-gym-multiband
pip install -e .
``` 

You will be able to run the [test](https://gitlab.com/IRO-Team/optical-rl-gym-multiband/-/tree/main/tests) right away. 

You can see the dependencies in the [setup.py](setup.py) file.

